﻿using BiMasa.Domain.DTO;
using BiMasa.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BiMasa.Service.Interfaces
{
    public interface IUserService
    {
        //Task<AuthenticateResponse> Login(AuthenticateRequest model);
        //Task<bool> Register(UserRegisterDTO user);
        Task<bool> UserExists(string username);
        Task<string> GetFullName(Guid userId);
        Task<UserDTO> GetUserWithEmail(string email);

        void Update(UserUpdateDTO model);
    }
}
