﻿using BiMasa.Domain.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BiMasa.Service.Interfaces
{
    public interface ICompanyService
    {
        Task<bool> AddCompany(CompanyRequest companyRequest);
    }
}
