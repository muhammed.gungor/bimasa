﻿using BiMasa.Domain.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BiMasa.Service.Interfaces
{
    public interface IAuthenticationService
    {
        Task<bool> PortalRegisterAsync(PortalRegistrationDTO registrationDTO);
        Task<AuthenticateResponse> LoginAsync(AuthenticateRequest authenticateRequest);
    }
}
