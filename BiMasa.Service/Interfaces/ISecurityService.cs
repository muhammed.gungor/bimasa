﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BiMasa.Service.Interfaces
{
    public interface ISecurityService
    {
        string Encrypt(string text);
        string Decrypt(string text);
        bool Verify(string password, string passwordHash);
    }
}
