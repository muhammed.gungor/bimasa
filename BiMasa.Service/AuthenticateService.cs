﻿using BiMasa.Common;
using BiMasa.Common.Helper;
using BiMasa.Core.Interfaces;
using BiMasa.Domain.DTO;
using BiMasa.Domain.Entities;
using BiMasa.Service.Interfaces;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BiMasa.Service
{
    public class AuthenticateService : IAuthenticationService
    {
        private readonly IUnitOfWork _uow;
        private readonly ISecurityService _securityService;
        private readonly AppSettings _appSettings;

        public AuthenticateService(IUnitOfWork unitOfWork, ISecurityService securityService, IOptions<AppSettings> appSettings)
        {
            _uow = unitOfWork;
            _securityService = securityService;
            _appSettings = appSettings.Value;
        }

        public async Task<AuthenticateResponse> LoginAsync(AuthenticateRequest authenticateRequest)
        {
            User user;

            if (authenticateRequest.Username.Contains("@"))
                user = await _uow.UserRepository.FindByEmailAsync(authenticateRequest.Username);
            else
                user = await _uow.UserRepository.FindByNameAsync(authenticateRequest.Username);

            // return null if user not found
            if (user == null) return null;

            if (!_securityService.Verify(authenticateRequest.Password, user.Password)) return null;

            // authentication successful so generate jwt token
            var token = GenerateJwtToken(user);

            return new AuthenticateResponse(user, token);
        }

        public async Task<bool> PortalRegisterAsync(PortalRegistrationDTO registrationDTO)
        {
            var isCompanyExist = await _uow.CompanyRepository.Get(c => c.Title.Trim() == registrationDTO.CompanyTitle.Trim());

            if (isCompanyExist != null)
                throw new Exception("Company is exist!");

            var isUserExist = await _uow.UserRepository.UserExists(registrationDTO.Email);

            if (isCompanyExist != null)
                throw new Exception("User is exist!");

            var company = new Company()
            {
                CountryId = 16,
                CreatedBy = registrationDTO.Email,
                Title = registrationDTO.CompanyTitle,
            };

            await _uow.CompanyRepository.AddAsync(company);

            var user = new User()
            {
                Company = company,
                Email = registrationDTO.Email,
                FirstName = registrationDTO.FirstName,
                LastName = registrationDTO.LastName,
                Password = "123456",
                Username = registrationDTO.Email
            };

            await _uow.UserRepository.AddAsync(user);

            return await _uow.SaveChangesAsync() > 0;
        }

        private string GenerateJwtToken(User user)
        {
            // generate token that is valid for 7 days
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", user.Id.ToString()) }),
                Expires = DateTime.UtcNow.AddDays(Constants.TOKEN_EXPIRE_DAYS),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
