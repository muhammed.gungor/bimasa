﻿using BiMasa.Core.Interfaces;
using BiMasa.Domain.DTO;
using BiMasa.Domain.Entities;
using BiMasa.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BiMasa.Service
{
    public class CompanyService : ICompanyService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICompanyRepository _companyRepository;
        public CompanyService(IUnitOfWork unitOfWork, ICompanyRepository companyRepository)
        {
            _companyRepository = companyRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> AddCompany(CompanyRequest companyRequest)
        {
            var company = new Company()
            {
                Address = companyRequest.Address,
                CountryId = companyRequest.CountryId,
                SinceDate = companyRequest.SinceDate,
                Title = companyRequest.Title
            };

            await _companyRepository.AddAsync(company);
            return await _unitOfWork.SaveChangesAsync() > 0;
        }
    }
}
