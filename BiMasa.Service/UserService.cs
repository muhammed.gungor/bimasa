﻿using BiMasa.Core.Interfaces;
using BiMasa.Domain.DTO;
using BiMasa.Domain.Entities;
using BiMasa.Service.Interfaces;
using BiMasa.Common;
using BiMasa.Common.Helper;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BiMasa.Service
{
    public class UserService : IUserService
    {
        private readonly AppSettings _appSettings;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ISecurityService _securityService;

        public UserService(IOptions<AppSettings> appSettings, IUnitOfWork unitOfWork, ISecurityService securityService)
        {
            _appSettings = appSettings.Value;
            _unitOfWork = unitOfWork;
            _securityService = securityService;
        }

        public void Update(UserUpdateDTO model)
        {
            User user =  _unitOfWork.UserRepository.GetByIdAsync(model.Id).Result;

            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.Email = model.Email;

            _unitOfWork.UserRepository.Update(user);
        }

        //public async Task<bool> Register(UserRegisterDTO userDTO)
        //{
        //    User user = new User()
        //    {
        //        FirstName = userDTO.FirstName,
        //        LastName = userDTO.LastName,
        //        Username = userDTO.Username,
        //        Password = _securityService.Encrypt("123456")
        //    };

        //    await _unitOfWork.UserRepository.AddAsync(user);
        //    return await _unitOfWork.SaveChangesAsync() > 0;
        //}

        public async Task<bool> UserExists(string username)
        {
            return await _unitOfWork.UserRepository.UserExists(username);
        }

        // helper methods

        

        public async Task<string> GetFullName(Guid userId)
        {
            return await _unitOfWork.UserRepository.GetFullName(userId);
        }

        public async Task<UserDTO> GetUserWithEmail(string email)
        {
            var user = await _unitOfWork.UserRepository.FindByEmailAsync(email);

            var userDto = new UserDTO(user);

            return userDto;                
        }
    }
}
