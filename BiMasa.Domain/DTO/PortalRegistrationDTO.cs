﻿using FluentValidation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BiMasa.Domain.DTO
{
    public class PortalRegistrationDTO
    {
        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [Required]
        [JsonProperty("companyTitle")]
        public string CompanyTitle { get; set; }

        [Required]
        [JsonProperty("companyAddress")]
        public string CompanyAddress { get; set; }

        [Required]
        [JsonProperty("companyPhone")]
        public string CompanyPhone { get; set; }

    }

    public class PortalRegistrationValidator : AbstractValidator<PortalRegistrationDTO>
    {
        public PortalRegistrationValidator()
        {
            RuleFor(x => x.Email).NotEmpty().WithMessage("Email is required.");
            RuleFor(x => x.Email).EmailAddress().WithMessage("Invalid email format.");
            RuleFor(x => x.CompanyAddress).NotEmpty().WithMessage("Company Address is required");
            RuleFor(x => x.FirstName).NotEmpty().WithMessage("First Name is required");
            RuleFor(x => x.LastName).NotEmpty().WithMessage("Last Name is required");
            RuleFor(x => x.CompanyTitle).NotEmpty().WithMessage("Company Title is required");;
        }
    }
}
