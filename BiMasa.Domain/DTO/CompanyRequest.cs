﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace BiMasa.Domain.DTO
{
    public class CompanyRequest
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("sinceDate")]
        public DateTime SinceDate { get; set; }

        [JsonProperty("countryId")]
        public int CountryId { get; set; }
    }
}
