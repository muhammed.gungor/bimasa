using System;
using System.Collections.Generic;
using System.Text;

namespace BiMasa.Domain.Entities
{
    public class Category : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Banner { get; set; }
        public Guid ParentId { get; set; }

        public Category Parent { get; set; }
        public List<Product> Products { get; set; }
    }
}
