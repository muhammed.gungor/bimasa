﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BiMasa.Domain.Entities
{
    public class Company : BaseEntity
    {
        public string Title { get; set; }
        public string Address { get; set; }
        public DateTime SinceDate { get; set; }
        public int CountryId { get; set; }
    }
}
