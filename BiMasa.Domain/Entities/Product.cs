﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BiMasa.Domain.Entities
{
    public class Product : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Banner { get; set; }
        public string Ingredients { get; set; }
        public Guid CategoryId { get; set; }

        public Category Category { get; set; }
    }
}
