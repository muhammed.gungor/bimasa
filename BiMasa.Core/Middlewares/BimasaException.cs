﻿using BiMasa.Domain.DTO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BiMasa.Core.Middlewares
{
    public class BimasaException
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public BimasaException(RequestDelegate next, ILoggerFactory logFactory)
        {
            _next = next;

            _logger = logFactory.CreateLogger("MyMiddleware");
        }

        public async Task Invoke(HttpContext httpContext)
        {
            string message = "";
            try
            {
                await _next.Invoke(httpContext);
            }
            catch (Exception ex)
            {
                message = ex.Message;
                var guid = Guid.NewGuid();

                httpContext.Items.Add("exception", ex);
                httpContext.Items.Add("exceptionMessage", message);
                httpContext.Items.Add("correlationId", guid);
                httpContext.Response.StatusCode = StatusCodes.Status400BadRequest;

                _logger.LogError(ex.Message, guid);
            }
        }
    }
    public static class BimasaExceptionExtension
    {
        public static IApplicationBuilder UseBimasaExceptionMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<BimasaException>();
        }
    }
}
