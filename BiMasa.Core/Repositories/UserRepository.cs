﻿using BiMasa.Core.Context;
using BiMasa.Core.Interfaces;
using BiMasa.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BiMasa.Common;
using BiMasa.Common.Helper;
using Microsoft.EntityFrameworkCore;

namespace BiMasa.Core.Repositories
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        private readonly ApplicationDbContext _dbContext;
        public UserRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
            this._dbContext = dbContext;
        }

        public async Task<User> FindByEmailAsync(string email)
        {
            return await _dbContext.Users.FirstOrDefaultAsync(c => c.Email == email);
        }

        public async Task<string> GetFullName(Guid userId)
        {            
            var user = await GetByIdAsync(userId);

            if (user == null)
                throw new Exception("User not found");

            return user.FirstName + " " + user.LastName;
        }

        public async Task<User> FindByNameAsync(string userName)
        {
            User user = await Get(e => e.Username.ToLower() == userName, null, "");

            return user;
        }

        public async Task<bool> UserExists(string username)
        {
            bool isExist =await Exist(e => e.Username.ToLower() == username.ToLower());

            return isExist;
        }        
    }
}
