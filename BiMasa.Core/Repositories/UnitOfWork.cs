﻿using BiMasa.Core.Context;
using BiMasa.Core.Interfaces;
using BiMasa.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiMasa.Core.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _dbContext;
        private CompanyRepository _companyRepository;
        private UserRepository _userRepository;
        //private bool disposed = false;

        public UnitOfWork(ApplicationDbContext databaseContext) => _dbContext = databaseContext;

        public ICompanyRepository CompanyRepository => _companyRepository = _companyRepository ?? new CompanyRepository(_dbContext);
        public IUserRepository UserRepository => _userRepository = _userRepository ?? new UserRepository(_dbContext);


        #region Core Database Processes

        public IBaseRepository<T> GetRepository<T>() where T : BaseEntity
        {
            return new BaseRepository<T>(_dbContext);
        }

        public void Rollback()
        {
            _dbContext.ChangeTracker
                    .Entries()
                    .ToList()
                    .ForEach(x => x.Reload());
        }

        public async Task<int> SaveChangesAsync()
        {
            try
            {
                return await _dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Save Changes Ex.", ex.InnerException);
            }
        }
        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}