﻿using BiMasa.Core.Context;
using BiMasa.Core.Interfaces;
using BiMasa.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BiMasa.Core.Repositories
{
    public class CompanyRepository : BaseRepository<Company>,ICompanyRepository
    {
        private readonly ApplicationDbContext _dbContext;
        public CompanyRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
            this._dbContext = dbContext;
        }

        public async Task<IEnumerable<Company>> GetCompanyListByCountryId(int countryId)
        {
            if (countryId < 1)
                throw new Exception("countryId parameter must be greater than one");

            var list = await GetMultiple(c => c.CountryId == countryId);
            
            return list;
        }

        public Task<Company> GetLastCompany()
        {
            throw new NotImplementedException();
        }
    }
}
