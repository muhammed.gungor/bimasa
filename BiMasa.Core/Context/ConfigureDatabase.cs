﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace BiMasa.Core.Context
{
    public class ConfigureDatabase
    {
        public static void Configure(IServiceProvider serviceProvider)
        {
            Configure(
                serviceProvider.GetRequiredService<ApplicationDbContext>(),
                serviceProvider.GetRequiredService<ILogger<ConfigureDatabase>>());
        }

        private static void Configure(ApplicationDbContext applicationDbContext, ILogger logger)
        {
            try
            {
                applicationDbContext.Database.Migrate();
                logger.LogInformation("Database Migration OK!");
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
            }
        }
    }
}
