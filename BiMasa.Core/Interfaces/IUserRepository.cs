﻿using BiMasa.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BiMasa.Core.Interfaces
{
    public interface IUserRepository : IBaseRepository<User>
    {
        Task<User> FindByEmailAsync(string email);
        Task<bool> UserExists(string username);
        Task<string> GetFullName(Guid userId);
        Task<User> FindByNameAsync(string userName);
    }
}
