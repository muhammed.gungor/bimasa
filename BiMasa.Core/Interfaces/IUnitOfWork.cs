﻿using BiMasa.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BiMasa.Core.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IBaseRepository<T> GetRepository<T>() where T : BaseEntity;
        ICompanyRepository CompanyRepository { get; }
        IUserRepository UserRepository { get; }
        Task<int> SaveChangesAsync();
        void Rollback();
    }
}
