﻿using BiMasa.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BiMasa.Core.Interfaces
{
    public interface ICompanyRepository : IBaseRepository<Company>
    {
        Task<Company> GetLastCompany();
        Task<IEnumerable<Company>> GetCompanyListByCountryId(int countryId);
    }
}
