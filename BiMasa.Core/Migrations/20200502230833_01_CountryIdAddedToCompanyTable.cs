﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BiMasa.Core.Migrations
{
    public partial class _01_CountryIdAddedToCompanyTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CountryId",
                table: "Companies",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CountryId",
                table: "Companies");
        }
    }
}
