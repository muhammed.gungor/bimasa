﻿using BiMasa.Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BiMasa.API.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class MyAuthorizeAttribute : Attribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var user = context.HttpContext.Items["User"] != null ? (User)context.HttpContext.Items["User"] : null;
            if (user == null)
            {
                // not logged in
                context.HttpContext.Items.Add("exception", "Unauthorized");
                context.HttpContext.Items.Add("exceptionMessage", "Unauthorized");
                context.HttpContext.Items.Add("correlationId", "");
                context.Result = new ObjectResult("")
                {
                    StatusCode = StatusCodes.Status401Unauthorized
                };
            }
        }
    }
}
