﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BiMasa.Domain.DTO;
using BiMasa.Service.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BiMasa.API.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly IAuthenticationService _authService;

        public AuthenticationController(IAuthenticationService authenticationService)
        {
            _authService = authenticationService;
        }

        [HttpPost("register")]        
        [Produces("application/json")]
        public async Task<IActionResult> CompanyRegister(PortalRegistrationDTO request)
        {
            return Ok(await _authService.PortalRegisterAsync(request));
        }

        [HttpPost("login")]
        [Produces("application/json")]
        public async Task<IActionResult> Login([FromBody] AuthenticateRequest model)
        {
            var response = await _authService.LoginAsync(model);
            if (response == null)
            {
                return Unauthorized("Username or password is incorrect");
            }
            return Ok();
        }
    }
}
