﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BiMasa.API.Filters;
using BiMasa.Core.Interfaces;
using BiMasa.Domain.DTO;
using BiMasa.Domain.Entities;
using BiMasa.Service.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BiMasa.API.Controllers
{
    [Route("api/user")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [MyAuthorize]
        [HttpPost("update")]
        public IActionResult Update([FromBody] UserUpdateDTO model)
        {
            _userService.Update(model);

            return Ok();
        }

        [MyAuthorize]
        [HttpGet("fullname")]
        public async Task<IActionResult> GetFullName()
        {
            User user =(User)HttpContext.Items["User"];
            return Ok(await _userService.GetFullName(Guid.NewGuid()));          
        }
    }
}