﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace BiMasa.Common.Helper
{
    public static class JsonHelper
    {
        public static bool ValidateJSON(this string s)
        {
            var response = false;
            try
            {
                JToken.Parse(s);
                response = true;
            }
            catch (JsonReaderException ex)
            {
                response = false;
            }
            return response;
        }
    }
}
