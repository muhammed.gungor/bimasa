﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BiMasa.Common.Helper
{
    public class AppSettings
    {
        public string Secret { get; set; }
        public string Key { get; set; }
        public int SortNumber { get; set; }
    }
}
